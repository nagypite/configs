set smartindent
set incsearch
set hlsearch
set smartcase
set shiftwidth=4
set tabstop=4
set noexpandtab
set nowrap


alias ..='cd ..'
alias ...='cd ../..'

alias la='ls -lha'

alias magickflags='Magick++-config --cppflags --cxxflags --ldflags --libs'

alias gis='git status'
alias gig='git grep --color'

alias gitt='git tag'

alias gicm='git checkout master'
alias gidm='git diff master'
alias gimm='git merge master'

alias giff='git flow feature'
alias gifr='git flow release'
alias gifh='git flow hotfix'

## outdated
#alias gic='git checkout'
#alias gid='git diff'
#alias gim='git merge'
#complete -o default -o nospace -F _git_checkout gic
#complete -o default -o nospace -F _git_merge gim
#complete -o default -o nospace -F _git_diff gid

## fix from http://git.661346.n2.nabble.com/Bash-tab-completion-for-git-fetch-alias-is-broken-on-Git-1-7-7-1-td6980366.html

__define_git_completion () {
	eval "
		_git_$2_shortcut () {
			COMP_LINE=\"git $2\${COMP_LINE#$1}\"
				let COMP_POINT+=$((4+${#2}-${#1}))
				COMP_WORDS=(git $2 \"\${COMP_WORDS[@]:1}\")
				let COMP_CWORD+=1

				local cur words cword prev
				_get_comp_words_by_ref -n =: cur words cword prev
				_git_$2
		}
	"
}

__git_shortcut () {
	type _git_$2_shortcut &>/dev/null || __define_git_completion $1 $2
		alias $1="git $2 $3"
		complete -o default -o nospace -F _git_$2_shortcut $1
}

__git_shortcut  gic    checkout
__git_shortcut  gid    diff
__git_shortcut  gim    merge
